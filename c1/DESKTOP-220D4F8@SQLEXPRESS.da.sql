/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0001
DATE:      01-23-2017 12:58:29
SERVER:    DESKTOP-220D4F8\SQLEXPRESS

DATABASE:	da
	Tables:
		Segment, Status report Nikola Antanasijevic


=============================================================*/
SET ANSI_WARNINGS ON
SET XACT_ABORT ON
SET ARITHABORT ON
SET NOCOUNT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
GO
-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[Segment]
Print 'Create Table [dbo].[Segment]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Segment] (
		[SegmentId]     [uniqueidentifier] NOT NULL,
		[Content]       [varbinary](max) NULL,
		CONSTRAINT [PK_Segment]
		PRIMARY KEY
		CLUSTERED
		([SegmentId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Segment]
	ADD
	CONSTRAINT [DF_Segment_SegmentId]
	DEFAULT (newsequentialid()) FOR [SegmentId]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SegmentMetadata]
	ON [dbo].[Segment] ([SegmentId])
	ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Segment] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- Create Table [dbo].[Status report Nikola Antanasijevic]
Print 'Create Table [dbo].[Status report Nikola Antanasijevic]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[Status report Nikola Antanasijevic] (
		[FIELD_1]     [varchar](10) NULL,
		[FIELD_2]     [varchar](168) NULL,
		[FIELD_3]     [varchar](26) NULL,
		[FIELD_4]     [varchar](21) NULL,
		[FIELD_5]     [varchar](10) NULL,
		[FIELD_6]     [varchar](28) NULL,
		[FIELD_7]     [varchar](19) NULL,
		[FIELD_8]     [varchar](11) NULL
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[Status report Nikola Antanasijevic] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
