/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0001
DATE:      01-23-2017 12:59:57
SERVER:    DESKTOP-220D4F8\SQLEXPRESS

DATABASE:	da
	Schemas:
		db_accessadmin, db_backupoperator, db_datareader, db_datawriter
		db_ddladmin, db_denydatareader, db_denydatawriter, db_owner, db_securityadmin
		dbo, guest, INFORMATION_SCHEMA, sys
	Tables:
		F, FrenchDataTest23, Roles, RunningJobs, Segment, ServerParametersInstance
		Status report Nikola Antanasijevic, Users


=============================================================*/
SET ANSI_WARNINGS ON
SET XACT_ABORT ON
SET ARITHABORT ON
SET NOCOUNT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
GO

USE [da]
GO
-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[F]
Print 'Create Table [dbo].[F]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[F] (
		[FIELD_1]      [varchar](10) NULL,
		[FIELD_2]      [text] NULL,
		[FIELD_3]      [varchar](226) NULL,
		[FIELD_4]      [varchar](10) NULL,
		[FIELD_5]      [varchar](16) NULL,
		[FIELD_6]      [varchar](10) NULL,
		[FIELD_7]      [varchar](112) NULL,
		[FIELD_8]      [varchar](10) NULL,
		[FIELD_9]      [varchar](19) NULL,
		[FIELD_10]     [varchar](10) NULL,
		[FIELD_11]     [varchar](148) NULL,
		[FIELD_12]     [varchar](10) NULL,
		[FIELD_13]     [varchar](128) NULL,
		[FIELD_14]     [varchar](10) NULL,
		[FIELD_15]     [varchar](14) NULL,
		[FIELD_16]     [varchar](10) NULL,
		[FIELD_17]     [varchar](14) NULL,
		[FIELD_18]     [varchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[F] SET (LOCK_ESCALATION = TABLE)
GO

-- Create Schema [db_denydatawriter]
Print 'Create Schema [db_denydatawriter]'
GO
-- Create Schema [db_denydatareader]
Print 'Create Schema [db_denydatareader]'
GO
-- Create Schema [db_owner]
Print 'Create Schema [db_owner]'
GO
-- Create Schema [db_backupoperator]
Print 'Create Schema [db_backupoperator]'
GO
-- Create Schema [db_datawriter]
Print 'Create Schema [db_datawriter]'
GO
-- Create Schema [db_accessadmin]
Print 'Create Schema [db_accessadmin]'
GO
-- Create Schema [db_securityadmin]
Print 'Create Schema [db_securityadmin]'
GO
-- Create Schema [db_datareader]
Print 'Create Schema [db_datareader]'
GO
-- Create Table [dbo].[FrenchDataTest23]
Print 'Create Table [dbo].[FrenchDataTest23]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FrenchDataTest23] (
		[TestData]     [varchar](50) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FrenchDataTest23] SET (LOCK_ESCALATION = TABLE)
GO

-- Create Schema [db_ddladmin]
Print 'Create Schema [db_ddladmin]'
GO
-- Create Table [dbo].[Roles]
Print 'Create Table [dbo].[Roles]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Roles] (
		[RoleID]          [uniqueidentifier] NOT NULL,
		[RoleName]        [nvarchar](260) NOT NULL,
		[Description]     [nvarchar](512) NULL,
		[TaskMask]        [nvarchar](32) NOT NULL,
		[RoleFlags]       [tinyint] NOT NULL,
		CONSTRAINT [PK_Roles]
		PRIMARY KEY
		NONCLUSTERED
		([RoleID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [IX_Roles]
	ON [dbo].[Roles] ([RoleName])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Roles] SET (LOCK_ESCALATION = TABLE)
GO

-- Create Table [dbo].[RunningJobs]
Print 'Create Table [dbo].[RunningJobs]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RunningJobs] (
		[JobID]            [nvarchar](32) NOT NULL,
		[StartDate]        [datetime] NOT NULL,
		[ComputerName]     [nvarchar](32) NOT NULL,
		[RequestName]      [nvarchar](425) NOT NULL,
		[RequestPath]      [nvarchar](425) NOT NULL,
		[UserId]           [uniqueidentifier] NOT NULL,
		[Description]      [ntext] NULL,
		[Timeout]          [int] NOT NULL,
		[JobAction]        [smallint] NOT NULL,
		[JobType]          [smallint] NOT NULL,
		[JobStatus]        [smallint] NOT NULL,
		CONSTRAINT [PK_RunningJobs]
		PRIMARY KEY
		CLUSTERED
		([JobID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_RunningJobsStatus]
	ON [dbo].[RunningJobs] ([ComputerName], [JobType])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[RunningJobs] SET (LOCK_ESCALATION = TABLE)
GO

-- Create Table [dbo].[Segment]
Print 'Create Table [dbo].[Segment]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Segment] (
		[SegmentId]     [uniqueidentifier] NOT NULL,
		[Content]       [varbinary](max) NULL,
		CONSTRAINT [PK_Segment]
		PRIMARY KEY
		CLUSTERED
		([SegmentId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Segment]
	ADD
	CONSTRAINT [DF_Segment_SegmentId]
	DEFAULT (newsequentialid()) FOR [SegmentId]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SegmentMetadata]
	ON [dbo].[Segment] ([SegmentId])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Segment] SET (LOCK_ESCALATION = TABLE)
GO

-- Create Table [dbo].[ServerParametersInstance]
Print 'Create Table [dbo].[ServerParametersInstance]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ServerParametersInstance] (
		[ServerParametersID]     [nvarchar](32) NOT NULL,
		[ParentID]               [nvarchar](32) NULL,
		[Path]                   [nvarchar](425) NOT NULL,
		[CreateDate]             [datetime] NOT NULL,
		[ModifiedDate]           [datetime] NOT NULL,
		[Timeout]                [int] NOT NULL,
		[Expiration]             [datetime] NOT NULL,
		[ParametersValues]       [image] NOT NULL,
		CONSTRAINT [PK_ServerParametersInstance]
		PRIMARY KEY
		CLUSTERED
		([ServerParametersID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ServerParametersInstanceExpiration]
	ON [dbo].[ServerParametersInstance] ([Expiration] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServerParametersInstance] SET (LOCK_ESCALATION = TABLE)
GO

-- Create Table [dbo].[Status report Nikola Antanasijevic]
Print 'Create Table [dbo].[Status report Nikola Antanasijevic]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Status report Nikola Antanasijevic] (
		[FIELD_1]     [varchar](10) NULL,
		[FIELD_2]     [varchar](168) NULL,
		[FIELD_3]     [varchar](26) NULL,
		[FIELD_4]     [varchar](21) NULL,
		[FIELD_5]     [varchar](10) NULL,
		[FIELD_6]     [varchar](28) NULL,
		[FIELD_7]     [varchar](19) NULL,
		[FIELD_8]     [varchar](11) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Status report Nikola Antanasijevic] SET (LOCK_ESCALATION = TABLE)
GO

-- Create Table [dbo].[Users]
Print 'Create Table [dbo].[Users]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users] (
		[UserID]       [uniqueidentifier] NOT NULL,
		[Sid]          [varbinary](85) NULL,
		[UserType]     [int] NOT NULL,
		[AuthType]     [int] NOT NULL,
		[UserName]     [nvarchar](260) NULL,
		CONSTRAINT [PK_Users]
		PRIMARY KEY
		NONCLUSTERED
		([UserID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [IX_Users]
	ON [dbo].[Users] ([Sid], [UserName], [AuthType])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Users] SET (LOCK_ESCALATION = TABLE)
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
