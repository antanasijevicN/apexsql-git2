/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0001
DATE:      01-23-2017 14:11:14
SERVER:    DESKTOP-220D4F8\SQLEXPRESS

DATABASE:	da
  Tables:  F


=============================================================*/
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_WARNINGS ON
SET NOCOUNT ON
SET XACT_ABORT ON
GO

USE [da]
GO

-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[F]
Print 'Create Table [dbo].[F]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[F] (
		[FIELD_1]      [varchar](10) NULL,
		[FIELD_2]      [text] NULL,
		[FIELD_3]      [varchar](226) NULL,
		[FIELD_4]      [varchar](10) NULL,
		[FIELD_5]      [varchar](16) NULL,
		[FIELD_6]      [varchar](10) NULL,
		[FIELD_7]      [varchar](112) NULL,
		[FIELD_8]      [varchar](10) NULL,
		[FIELD_9]      [varchar](19) NULL,
		[FIELD_10]     [varchar](10) NULL,
		[FIELD_11]     [varchar](148) NULL,
		[FIELD_12]     [varchar](10) NULL,
		[FIELD_13]     [varchar](128) NULL,
		[FIELD_14]     [varchar](10) NULL,
		[FIELD_15]     [varchar](14) NULL,
		[FIELD_16]     [varchar](10) NULL,
		[FIELD_17]     [varchar](14) NULL,
		[FIELD_18]     [varchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[F] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
