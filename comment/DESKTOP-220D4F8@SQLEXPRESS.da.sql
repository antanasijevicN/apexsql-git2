/*=============================================================
SCRIPT HEADER

VERSION:   1.01.0001
DATE:      01-23-2017 12:09:13
SERVER:    DESKTOP-220D4F8\SQLEXPRESS

DATABASE:	da
	Tables:
		FrenchDataTest23, ServerParametersInstance


=============================================================*/
SET ANSI_WARNINGS ON
SET XACT_ABORT ON
SET ARITHABORT ON
SET NOCOUNT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
GO
-- BEGINNING TRANSACTION STRUCTURE
PRINT 'Beginning transaction STRUCTURE'
BEGIN TRANSACTION _STRUCTURE_
GO
-- Create Table [dbo].[FrenchDataTest23]
Print 'Create Table [dbo].[FrenchDataTest23]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[FrenchDataTest23] (
		[TestData]     [varchar](50) NULL
) ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[FrenchDataTest23] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- Create Table [dbo].[ServerParametersInstance]
Print 'Create Table [dbo].[ServerParametersInstance]'
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE TABLE [dbo].[ServerParametersInstance] (
		[ServerParametersID]     [nvarchar](32) NOT NULL,
		[ParentID]               [nvarchar](32) NULL,
		[Path]                   [nvarchar](425) NOT NULL,
		[CreateDate]             [datetime] NOT NULL,
		[ModifiedDate]           [datetime] NOT NULL,
		[Timeout]                [int] NOT NULL,
		[Expiration]             [datetime] NOT NULL,
		[ParametersValues]       [image] NOT NULL,
		CONSTRAINT [PK_ServerParametersInstance]
		PRIMARY KEY
		CLUSTERED
		([ServerParametersID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
CREATE NONCLUSTERED INDEX [IX_ServerParametersInstanceExpiration]
	ON [dbo].[ServerParametersInstance] ([Expiration] DESC)
	ON [PRIMARY]
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER TABLE [dbo].[ServerParametersInstance] SET (LOCK_ESCALATION = TABLE)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

-- COMMITTING TRANSACTION STRUCTURE
PRINT 'Committing transaction STRUCTURE'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _STRUCTURE_
GO

SET NOEXEC OFF
GO
